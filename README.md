Commencer par exécuter la commande `pip3 install -r requirements.txt` pour installer les dépendances pip3

Nous modélisons trois réseaux de neuronnes: Alice, Bob et Eve. Alice doit pouvoir transmettre à Bob le message P sur un canal public, sans qu'Eve puisse le déchiffrer. 
Pour cela, Alice et Bob partagent la même clé K.

Le but est de modéliser ces trois réseaux de neuronnes et d'observer les résultats sur TensorBoard.

Le message P et la clé K seront modélisés par des matrices de taille (taille batch * taille message) contenant des entiers aléatoires entre 0 et 1.

# 1) Les entrées
Au lancement du programme, on demande à l'utilisateur :
*  Le nombre d'Epochs
*  Le taille du batch
*  La taille du message/clé qui est forcément la même, multiple de 8

# 2) Création du dossier de logs
Afin d'utilisateur TensorBoard par la suite, il est nécessaire de créer un dossier de logs dans lequel se trouvera les logs nommés en fonction de la date et de l'heure.

# 3) Création des modèles
Nous créons les 3 réseaux de neuronnes représentant Alice, Bob et Eve à l'aide d'un modèle séquentiel à une couche : la couche Dense.
Les trois modèles sont identiques à part pour Eve, qui elle ne possède que le message chiffré en entrée, et pas la clé K.

# 4) Entraînement des modèles
Pour le nombre d'Epochs définis par l'utilisateur, nous définissons les entrées et sorties des différentes couches, à part le message d'Alice P et la clé K qui ne varient pas.

La loss d'Alice est calculée à partir d'un tableau d'entiers aléatoires entre 0 et 1 et de taille la longueur du message.

Les loss de Bob et Eve sont calculées à partir du message d'entrée d'Alice.

On entraîne ensuite les modèles à l'aide de la fonction d'optimisation "Adam".

# 5) TensorBoard
Les résulats sont enregistrés dans le dossier de log à l'aide de tf.summary.scalar

Pour lancer TensorBoard et voir les résultats, en se plaçant dans le répertoire où est le main.py, utiliser la commande suivante :

`tensorboard --logdir logs/`

Nous observons que la loss d'Alice descend très rapidement, celle de Bob de façon plus lente, et celle d'Eve croît, ce qui est cohérent.