import tensorflow as tf
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
import datetime
import os

#Inputs
len_message=1

print("Epochs: (5000)")
nb_epoch=int(input())
print("Taille du batch: (30)")
len_batch=int(input())
print("Taille du message/clé: (16)")
len_message=int(input())

#Definition du nombre d'epoch, du message et de la clé
EPOCHS = nb_epoch
ALICE_IN_KEY=np.random.randint(2, size=(len_batch,len_message))
ALICE_IN_MESSAGE=np.random.randint(2, size=(len_batch,len_message))

#Definition des paths et creation du dossier de log
path="logs"
if (os.path.exists(path)) == False:
    os.mkdir(path)
current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
summary_writer = tf.summary.create_file_writer('logs/train_' + current_time)

#Definition des optimizers, model et loss function
optimizers = tf.keras.optimizers.Adam()
loss_fn = tf.losses.mean_absolute_error
combined_message = tf.concat([ALICE_IN_MESSAGE, ALICE_IN_KEY], axis=1)

#Creation du modele de Alice
model=Sequential()
model.add(Dense(len_message, input_shape=(len_message*2,)))

#Creation du modele de Bob
model1=Sequential()
model1.add(Dense(len_message, input_shape=(len_message*2,)))

#Creation du modele de Eve
model2=Sequential()
model2.add(Dense(len_message, input_shape=(len_message,)))

#On ne fournit pas la clé à Eve, c'est pourquoi len_message n'est pas multiplié par 2

for epoch in range(EPOCHS):
    with tf.GradientTape(persistent=True) as tape:
        y=np.ones((1,len_message))
        #Alice
        alice_out_message = model(combined_message)
        alice_loss = loss_fn(alice_out_message, y)
        
        #Bob
        bob_in_message = tf.concat([alice_out_message, ALICE_IN_KEY], axis=1)
        bob_out_message = model1(bob_in_message)
        bob_loss = loss_fn(bob_out_message, tf.cast(ALICE_IN_MESSAGE, 'float32'))
        
        #Eve
        eve_in_message = alice_out_message
        eve_out_message = model2(eve_in_message)
        eve_loss = loss_fn(eve_out_message, tf.cast(ALICE_IN_MESSAGE, 'float32'))
        
    grads= tape.gradient(alice_loss,model.trainable_variables)
    optimizers.apply_gradients(zip(grads,model.trainable_variables))
    
    grads1= tape.gradient(bob_loss,model1.trainable_variables)
    optimizers.apply_gradients(zip(grads1,model1.trainable_variables))
    
    grads2= tape.gradient(eve_loss,model2.trainable_variables)
    optimizers.apply_gradients(zip(grads2,model2.trainable_variables))
    
    with summary_writer.as_default():
        tf.summary.scalar('alice_loss',alice_loss.numpy()[0],step=epoch)
        tf.summary.scalar('bob_loss',bob_loss.numpy()[0],step=epoch)
        tf.summary.scalar('eve_loss',eve_loss.numpy()[0],step=epoch)
